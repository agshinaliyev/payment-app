package com.example.userms.dto;

import com.example.userms.entity.Authority;
import lombok.Data;

import java.util.Set;

@Data
public class UserResponse {
    Long id;
    String username;
    String firstName;
    String lastName;
    String email;
    String password;
    String phoneNumber;
    Set<Authority> authorities;
}
