package com.example.userms.dto;

//import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class UserRequest {

    String username;
    String firstName;
    String lastName;
    String email;
//    @Size(min = 4,max = 10)
    String password;
    String phoneNumber;

}
