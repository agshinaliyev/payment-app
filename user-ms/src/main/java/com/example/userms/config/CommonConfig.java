package com.example.userms.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@Import({com.example.common.exception.GlobalExceptionHandler.class})
public class CommonConfig {
}
