package com.example.userms.service;

import com.example.userms.dto.UserRequest;
import com.example.userms.dto.UserResponse;
import com.example.userms.entity.Authority;
import com.example.userms.entity.User;
import com.example.userms.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    @Override
    public UserResponse createUser(UserRequest request) {
        Authority authority =new Authority();
        authority.setAuthority("USER");
        User user = modelMapper.map(request, User.class);
        user.setAuthorities(Set.of(authority));

        return modelMapper.map(userRepository.save(user),UserResponse.class);
    }

    @Override
    public UserResponse getUser(Long id) {
        User user = userRepository.findById(id).orElseThrow(RuntimeException::new);
        return modelMapper.map(user, UserResponse.class);
    }

    @Override
    public List<UserResponse> listUsers() {
        return userRepository.findAll()
                .stream()
                .map((user -> modelMapper.map(userRepository.save(user),UserResponse.class)))
                .collect(Collectors.toList());
    }

    @Override
    public UserResponse updateUser(Long id, UserRequest body) {
         userRepository.findById(id).orElseThrow(RuntimeException::new);
        User user = modelMapper.map(body, User.class);
        user.setId(id);
        return modelMapper.map(userRepository.save(user),UserResponse.class);
    }

    @Override
    public void deleteUser(Long id) {

        User user = userRepository.findById(id).orElseThrow(RuntimeException::new);

        userRepository.delete(user);


    }
}
