package com.example.userms.service;

import com.example.userms.dto.UserRequest;
import com.example.userms.dto.UserResponse;

import java.util.List;

public interface UserService {
    UserResponse createUser(UserRequest request);
    UserResponse getUser(Long id);
    List<UserResponse> listUsers();
    UserResponse updateUser(Long id,UserRequest body);
    void deleteUser(Long id);



}
