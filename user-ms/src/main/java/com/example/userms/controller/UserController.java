package com.example.userms.controller;

import com.example.userms.dto.UserRequest;
import com.example.userms.dto.UserResponse;
import com.example.userms.service.UserServiceImpl;
//import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserServiceImpl userService;

    @PostMapping("/create")
    public ResponseEntity<UserResponse> createUser(@RequestBody UserRequest request) {
//
        return ResponseEntity.ok(userService.createUser(request));

    }

    @GetMapping("/get/{id}")
    public ResponseEntity<UserResponse> getUser(@PathVariable Long id) {

        return ResponseEntity.ok(userService.getUser(id));
    }
    @GetMapping("/list")
    public ResponseEntity<List<UserResponse>> listUser() {

        return ResponseEntity.ok(userService.listUsers());
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<UserResponse> updateUser(@PathVariable Long id,
                                                   @RequestBody UserRequest body){

        return ResponseEntity.ok(userService.updateUser(id,body));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        userService.deleteUser(id);

        return ResponseEntity.noContent().build();
    }



}
