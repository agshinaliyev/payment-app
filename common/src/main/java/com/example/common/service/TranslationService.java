package com.example.common.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Slf4j
@Service
@RequiredArgsConstructor
public class TranslationService {

    private final MessageSource messageSource;

    public String findByKey(String key,String lang ,Object ... args){

        try {
            String message = messageSource.getMessage(key, args, new Locale(lang, lang));
            return message;
        }catch (NoSuchMessageException nsm){

            return key;
        }

    }



}
