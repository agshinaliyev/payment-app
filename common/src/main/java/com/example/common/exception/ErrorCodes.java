package com.example.common.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum ErrorCodes {

    USER_NOT_FOUND("User not found");

    public final String code;


}
