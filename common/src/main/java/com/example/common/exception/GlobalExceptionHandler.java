package com.example.common.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

import static com.example.common.exception.ErrorCodes.USER_NOT_FOUND;

@RestControllerAdvice
public class GlobalExceptionHandler extends RuntimeException {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorResponseDto> handle(BadRequestException bad, WebRequest request) {
        bad.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(USER_NOT_FOUND.name())
                .build());


    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDto> handleMethodArgNotValidEx(MethodArgumentNotValidException ex,
                                                                      WebRequest request) {
        ex.printStackTrace();

        ErrorResponseDto response = ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .build();


        ex.getBindingResult().getFieldErrors()
                .stream()
                .forEach(error -> {
                    Map<String, String> data = response.getData();
                    data.put(error.getField(), error.getDefaultMessage());


                });

        return ResponseEntity.status(400).body(response);

    }


}
